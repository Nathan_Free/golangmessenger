-- Database: Messaging

-- DROP DATABASE "Messaging";

/*
CREATE DATABASE "Messaging"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
*/

DROP TABLE IF EXISTS users CASCADE;

CREATE TABLE IF NOT EXISTS users (
	id	serial PRIMARY KEY,
    first_name varchar(200) NOT NULL,
    last_name varchar(200) NOT NULL,
    email varchar(200) UNIQUE NOT NULL,
    password varchar(200) NOT NULL
);

DROP TABLE IF EXISTS message CASCADE;

CREATE TABLE IF NOT EXISTS message (
	id serial PRIMARY KEY,
    sender_id int references users(id),
    time_sent timestamp NOT NULL DEFAULT NOW(), 
    message_body varchar(1024)
);

DROP TABLE IF EXISTS friendship CASCADE;

CREATE TABLE IF NOT EXISTS friendship (
    sender_id int references users(id),
    recipient_id int references users(id),
    date_accepted timestamp NOT NULL DEFAULT NOW(),
    PRIMARY KEY(sender_id,recipient_id)
);

DROP TABLE IF EXISTS message_recipient CASCADE;

CREATE TABLE IF NOT EXISTS  message_recipient (
    recipient_id int references users(id),
    message_id int references message(id),
    has_read boolean NOT NULL DEFAULT FALSE,
    PRIMARY KEY(recipient_id,message_id)
);

DROP TABLE IF EXISTS friendship_request CASCADE;

CREATE TABLE IF NOT EXISTS friendship_request ( 
	sender_id int references users(id),
    recipient_id int references users(id),
    date_sent timestamp NOT NULL DEFAULT NOW(),
    PRIMARY KEY(sender_id,recipient_id) 
);

-- VIEWS 
CREATE VIEW messages_between_users AS SELECT message.sender_id, message_recipient.recipient_id, message.time_sent, message.message_body FROM message INNER JOIN message_recipient ON message.id = message_recipient.message_id;


-- INSERTIONS
INSERT INTO users (first_name,last_name,email,password) VALUES (
	'Daffy',
    'Duck',
    'daffyduck@gmail.com',
    '123qwe'
) ON CONFLICT DO NOTHING;

INSERT INTO users (first_name,last_name,email,password) VALUES (
	'Elemer',
    'Fudd',
    'elmerfudd@gmail.com',
    '123qwe'
) ON CONFLICT DO NOTHING;

INSERT INTO users (first_name,last_name,email,password) VALUES (
	'Sergeant',
    'Pepper',
    'sergeantpepper@gmail.com',
    '123qwe'
) ON CONFLICT DO NOTHING;

INSERT INTO message (sender_id,message_body) VALUES (
    '1', 'hello world'
    ) ON CONFLICT DO NOTHING;
    
INSERT INTO friendship_request (sender_id,recipient_id) VALUES (
    '1', '2'
    ) ON CONFLICT DO NOTHING;

INSERT INTO friendship_request (sender_id,recipient_id) VALUES (
    '1', '3'
    ) ON CONFLICT DO NOTHING;
    
INSERT INTO message_recipient (recipient_id,message_id) VALUES (
    '1', '1'
    ) ON CONFLICT DO NOTHING;
    
-- PROCEDURES 

    -- Procedure to do when a friendship_request is accepted by a user
    CREATE OR REPLACE FUNCTION accept_friendship(senderId int, recipientId int) 
    RETURNS void AS $$
    BEGIN
      
      DELETE FROM friendship_request 
			WHERE sender_id = senderId AND recipient_id = recipientId;
    
	  INSERT INTO friendship (sender_id,recipient_id) 
      		VALUES (senderId, recipientId) 
        	ON CONFLICT DO NOTHING;
            
    END;
    $$ LANGUAGE plpgsql;
    
-- UPDATES 

SELECT accept_friendship(1,2);


-- SELECT
SELECT * FROM friendship;



