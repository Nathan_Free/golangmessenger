# README #

Project to learn Go for server-side scripting as well as web-based back-end development. There is a lot of room for this project to grow and there is a good bit of code redundancy to address in the future, but much functionality is complete.

### What is this repository for? ###

* Instant Messaging Application with Golang
* Version 0.8

### How do I get set up? ###

* Go code to be built and resulting binary file to be run on a machine with redis, postgres, and golang installed and configured as normal 

* Though localhost may be used, so too can a Virtual Private Server (VPS) deploying to a registered domain server (SSL keys highly recommended). For my purposes, I purchased a Digital Ocean "Droplet" (essentially a VPS) and pushed it to a GoDaddy VPS.

* Dependencies: "github.com/gin-contrib/sessions", "github.com/gin-gonic/gin", "github.com/olahol/melody", Redis, Postgres, Golang, Ubuntu

* Simply run the main.sql file to get started with a database complete with sample data

* Build the binary file produced by Golang and run it to start the webserver

### Who do I talk to? ###

* For questions, contact Nathan Free at nathan.free.e205@gmail.com