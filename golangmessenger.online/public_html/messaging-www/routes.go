package main

func initalizeRoutes() {
	router.GET("/", renderIndex)
	router.POST("/login", loginHandler)
	//router.GET("/logout", logoutHandler)
	//router.POST("/registerpage", registerHandler)
	router.GET("/mainpage", renderMainPage)
	router.GET("/friends", renderFriendsPage)
	router.GET("/conversationpage/:UserID", renderConversationWithUser)
	router.POST("/sendMessage", SendMessageHandler)
	router.GET("/register", renderRegister)
	router.POST("/register/registerUser", registerUserHandler)
	// new websocket route
	router.GET("/websocket", websocketHandler)
	router.POST("/logout", logoutHandler)
	router.POST("/sendFriendRequest", sendFriendRequestHandler)
	router.POST("/respondToFriendRequest", respondToFriendRequestHandler)
	router.POST("/removeFriend", removeFriendHandler)
}
