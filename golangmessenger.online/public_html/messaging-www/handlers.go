package main

import (
	"fmt"
	"log"
	"net/http"

	"../messaging"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/olahol/melody"
)

var (
	store, err = sessions.NewRedisStore(10, "tcp", "127.0.0.1:6379", "+%?$7(W+9y[]r6FL", []byte("secret"))
)

func renderIndex(c *gin.Context) {
	render(c, gin.H{"title": "Login | Golang Messenger"}, "index.html")
	//renderStaticHTMLForRoot(c)
}

func renderRegister(c *gin.Context) {
	render(c, gin.H{"title": "Register | Golang Messenger"}, "registerpage.html")
}

func loginHandler(c *gin.Context) {
	r := c.Request
	dbConn := messaging.ConnectToDB()
	defer dbConn.Close()
	email := r.FormValue("email")
	password := r.FormValue("password")

	err := Login(dbConn, email, password)

	if err != nil {
		log.Println(err)
		c.Redirect(http.StatusFound, "/")
	} else {
		log.Println("loginHandler succeeded!")
		setSession(c, email, false)
	}
}

func logoutHandler(c *gin.Context) {
	//TODO: destroy redis session
	c.Redirect(http.StatusFound, "/")

}
func websocketHandler(c *gin.Context) {
	mel := melody.New()
	//https://sinister.ly/Thread-Tutorial-Building-real-time-web-apps-with-Go
	// let Melody handle it
	mel.HandleRequest(c.Writer, c.Request)
}

func registerUserHandler(c *gin.Context) {
	r := c.Request
	dbConn := messaging.ConnectToDB()
	defer dbConn.Close()
	firstName := r.FormValue("first_name")
	lastName := r.FormValue("last_name")
	email := r.FormValue("email")
	password := r.FormValue("password")

	err := messaging.RegisterUser(dbConn, firstName, lastName, email, password)

	if err != nil {
		log.Println(err)
		c.Redirect(http.StatusFound, "/register")
	} else {
		log.Println("Register succeeded!")
		redirectToPage(c, "/")
	}

}

func setSession(c *gin.Context, email string, conversationSelected bool) {
	w := c.Writer
	r := c.Request
	session, err := store.Get(r, "golangmessenger.online")
	if err != nil {
		output := fmt.Sprintf("Error fetching session: %s", err.Error())
		log.Println(output)
		return
	}
	log.Println("setSession email: " + email)
	session.Values["email"] = email
	//session.Values["role"] = "visitor"
	session.Save(r, w)

	//c.Redirect(http.StatusFound, "/mainpage")
	redirectToPage(c, "/mainpage")
}

//SendMessageHandler fires when a message is sent.
func SendMessageHandler(c *gin.Context) {
	r := c.Request
	dbConn := messaging.ConnectToDB()
	defer dbConn.Close()
	messageBody := r.FormValue("messageBody")
	senderID := r.FormValue("senderID")
	recipientID := r.FormValue("recipientID")

	//err := SendMessageToUser(dbConn, senderID, recipientID, messageBody)
	err := messaging.SendMessage(dbConn, senderID, recipientID, messageBody)
	if err != nil {
		c.Redirect(http.StatusFound, "/")
	} else {
		urlToRedirectTo := "/conversationpage/" + recipientID
		redirectToPage(c, urlToRedirectTo)
	}

}

//sendFriendRequestHandler fires when a user sends a friend request to another user.
func sendFriendRequestHandler(c *gin.Context) {
	r := c.Request
	dbConn := messaging.ConnectToDB()
	defer dbConn.Close()
	friendID := r.FormValue("userToSendTo")

	session, err := store.Get(r, "golangmessenger.online")
	if err != nil {
		output := fmt.Sprintf("Error getting session: %s", err.Error())
		log.Println(output)
		return
	}

	email := fmt.Sprintf("%s", session.Values["email"])

	senderID, err := messaging.GetIDFromEmail(dbConn, email)
	if err != nil {
		output := fmt.Sprintf("Error getting ID from email:  %s", err.Error())
		log.Println(output)
		return
	}

	err = messaging.SendFriendshipRequest(dbConn, senderID, friendID)
	if err != nil {
		log.Println("error with send friend request handler: ", err)
	} else {
		redirectToPage(c, "/friends")
	}
}

//removeFriendHandler fires when someone removes a friend from their friends list
func removeFriendHandler(c *gin.Context) {
	r := c.Request
	dbConn := messaging.ConnectToDB()
	defer dbConn.Close()
	friendID := r.FormValue("removeFriendButton")

	session, err := store.Get(r, "golangmessenger.online")
	if err != nil {
		output := fmt.Sprintf("Error getting session: %s", err.Error())
		log.Println(output)
		return
	}

	email := fmt.Sprintf("%s", session.Values["email"])

	currentUserID, err := messaging.GetIDFromEmail(dbConn, email)
	if err != nil {
		output := fmt.Sprintf("Error getting ID from email:  %s", err.Error())
		log.Println(output)
		return
	}

	err = messaging.RemoveFriend(dbConn, friendID, currentUserID)
	if err != nil {
		log.Println("error with removing friendship: ", err)
		return
	}

	urlToRedirectTo := "/mainpage"
	redirectToPage(c, urlToRedirectTo)
}

//respondToFriendRequestHandler fires when someone confirms or denies a friend request from friendpage
func respondToFriendRequestHandler(c *gin.Context) {
	r := c.Request
	dbConn := messaging.ConnectToDB()
	defer dbConn.Close()
	friendID := r.FormValue("friendID")
	log.Println(friendID)

	session, err := store.Get(r, "golangmessenger.online")
	if err != nil {
		output := fmt.Sprintf("Error getting session: %s", err.Error())
		log.Println(output)
		return
	}

	email := fmt.Sprintf("%s", session.Values["email"])

	currentUserID, err := messaging.GetIDFromEmail(dbConn, email)
	if err != nil {
		output := fmt.Sprintf("Error getting ID from email:  %s", err.Error())
		log.Println(output)
		return
	}

	responseType := r.FormValue("RequestResponse")

	if responseType == "Confirm" {
		err = messaging.AcceptFriendshipRequest(dbConn, friendID, currentUserID)
		if err != nil {
			log.Println("error with accepting friendship request: ", err)
			return
		}
	} else if responseType == "Deny" {
		err = messaging.RemoveFriendshipRequest(dbConn, friendID, currentUserID)
		if err != nil {
			log.Println("error with removing friendship request: ", err)
			return
		}
	} else {
		log.Println("response type is an unexpected value: ", responseType)
		return
	}
	redirectToPage(c, "/friends")
}

func redirectToPage(c *gin.Context, routeRelativeURL string) {
	c.Redirect(http.StatusFound, routeRelativeURL)
}

func renderMainPage(c *gin.Context) {
	dbConn := messaging.ConnectToDB()
	defer dbConn.Close()
	r := c.Request
	session, err := store.Get(r, "golangmessenger.online")
	if err != nil {
		output := fmt.Sprintf("Error getting session: %s", err.Error())
		log.Println(output)
		return
	}

	_, userStatus := session.Values["email"]
	if !userStatus {
		c.Redirect(http.StatusFound, "/")
	}

	email := fmt.Sprintf("%s", session.Values["email"])
	log.Println(email)

	contacts, err := messaging.GetUserFriendshipsFromEmail(dbConn, email)
	if err != nil {
		output := fmt.Sprintf("Error getting friendships: %s", err)
		log.Println(output)
		return
	}

	usersFullName, err := messaging.GetFullNameFromEmail(dbConn, email)
	if err != nil {
		output := fmt.Sprintf("Error getting username: %s", err)
		log.Println(output)
		return
	}

	render(c, gin.H{"payload": *contacts, "payload2": usersFullName}, "mainpage.html")
	//render(c, gin.H{"payload": account, "payloadtwo": contacts, "payloadmessage": messages}, "account.html")

}

func renderFriendsPage(c *gin.Context) {
	dbConn := messaging.ConnectToDB()
	defer dbConn.Close()
	r := c.Request
	session, err := store.Get(r, "golangmessenger.online")
	if err != nil {
		output := fmt.Sprintf("Error getting session: %s", err.Error())
		log.Println(output)
		return
	}

	_, userStatus := session.Values["email"]
	if !userStatus {
		c.Redirect(http.StatusFound, "/")
	}

	email := fmt.Sprintf("%s", session.Values["email"])

	userID, err2 := messaging.GetIDFromEmail(dbConn, email)
	if err2 != nil {
		log.Println("Error getting user ID: ", err2)
		return
	}

	notContactsList, err := messaging.GetAllExceptFriends(dbConn, userID)
	if err != nil {
		output := fmt.Sprintf("Error! I think saying more is redundant?")
		log.Println(output)
		return
	}

	requestsFromList, err := messaging.GetAllRequestsFrom(dbConn, userID)
	if err != nil {
		return
	}

	requestsToList, err := messaging.GetAllRequestsTo(dbConn, userID)
	if err != nil {
		return
	}

	render(c, gin.H{"userID": userID, "notContactsList": notContactsList, "requestsFromList": requestsFromList, "requestsToList": requestsToList}, "friendpage.html")
}

func renderConversationWithUser(c *gin.Context) {
	dbConn := messaging.ConnectToDB()
	defer dbConn.Close()
	r := c.Request
	session, err := store.Get(r, "golangmessenger.online")
	if err != nil {
		output := fmt.Sprintf("Error getting session: %s", err.Error())
		log.Println(output)
		return
	}

	_, userStatus := session.Values["email"]
	if !userStatus {
		c.Redirect(http.StatusFound, "/")
	}

	email := fmt.Sprintf("%s", session.Values["email"])
	log.Println(email)

	contacts, err := messaging.GetUserFriendshipsFromEmail(dbConn, email)
	if err != nil {
		output := fmt.Sprintf("Error getting friendships: %s", err)
		log.Println(output)
		return
	}

	usersFullName, err := messaging.GetFullNameFromEmail(dbConn, email)
	if err != nil {
		output := fmt.Sprintf("Error getting username: %s", err)
		log.Println(output)
		return
	}

	userID, err := messaging.GetIDFromEmail(dbConn, email)
	if err != nil {
		log.Println("Error getting id from email: ", err)
		return
	}

	recipientID := r.URL.Path[len("/conversationpage/"):]

	recipientName, err := messaging.GetFullNameFromID(dbConn, recipientID)

	messageList, err := messaging.GetAllMessagesBetweenTwoUsers(dbConn, userID, recipientID)
	if err != nil {
		log.Println("Error getting messages between users: ", err)
		return
	}

	potentialFriendList, err := messaging.GetAllExceptFriends(dbConn, userID)
	if err != nil {
		log.Println("Error getting potential friends: ", err)
		return
	}

	render(c, gin.H{"payload": *contacts, "payload2": usersFullName, "payload3": *messageList, "friendName": recipientName, "friendID": recipientID, "userID": userID, "userList": potentialFriendList}, "conversationpage.html")
}
