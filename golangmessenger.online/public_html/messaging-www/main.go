package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	//"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/olahol/melody"
	//"../messaging"
)

var router *gin.Engine

func main() {
	router = gin.Default()
	router.LoadHTMLGlob("templates/*")
	router.StaticFS("/assets", http.Dir("./assets"))

	//*********** idea taken from https://sinister.ly/Thread-Tutorial-Building-real-time-web-apps-with-Go for learning. unfinished.
	mel := melody.New() // melody middleware
	//Melody message handler
	mel.HandleMessage(func(ses *melody.Session, msg []byte) {
		// broadcast message to connected sockets
		mel.Broadcast(msg)
	})
	//***********
	initalizeRoutes()

	router.Run(":8080")
}

func render(c *gin.Context, data gin.H, templateName string) {
	switch c.Request.Header.Get("Accept") {
	case "application/json":
		c.JSON(http.StatusOK, data["payload"])

	case "application/xml":
		c.XML(http.StatusOK, data["payload"])

	default:
		c.HTML(http.StatusOK, templateName, data)
	}
}

// func renderStaticHTMLForRoot(c *gin.Context) {
// 	r := gin.Default()
// 	r.Use(static.Serve("/", static.LocalFile("index.html", false)))
// }

//Login allows user to login with email and pass. it does not create the user session as that is done elsewhere
func Login(dbconn *sql.DB, email string, password string) error {
	var storedPassword string

	if !verifyAccount(dbconn, email) {
		return fmt.Errorf("invalid account")
	}

	qry := fmt.Sprintf("select password from users where email='%s'", email)
	dbconn.QueryRow(qry).Scan(&storedPassword)

	if password != storedPassword {
		return fmt.Errorf("invalid credentials")
	}
	return nil
}

func verifyAccount(dbconn *sql.DB, emailaddress string) bool {
	err := dbconn.Ping()
	if err != nil {
		log.Println("could not ping db")
	} else {
		log.Println("db accessible")
	}
	var email string
	query := fmt.Sprintf("select email from users where email='%s'", emailaddress)
	dbconn.QueryRow(query).Scan(&email)
	if email == "" {
		return false
	}
	log.Println("user email: ", emailaddress)
	return true
}

//SendMessageToUser allows sending message but doesn't help in refreshing the page.
func SendMessageToUser(db *sql.DB, senderID string, recipientID string, messageBody string) error {
	return nil
	// dbConn := messaging.ConnectToDB()
	// defer dbConn.Close()
	// r := c.Request
	// session, err := store.Get(r, "golangmessenger.online")
	// if err != nil {
	// 	output := fmt.Sprintf("Error getting session: %s", err.Error())
	// 	log.Println(output)
	// 	return err
	// }

	// _, userStatus := session.Values["email"]
	// if !userStatus {
	// 	c.Redirect(http.StatusFound, "/")
	// }

	// err = messaging.SendMessage(dbConn, senderID, recipientID, messageBody)
	// if err != nil {
	// 	output := fmt.Sprintf("Error sending message: %s", err.Error())
	// 	log.Println(output)
	// 	return err
	// }
	// 	log.Println("message send success")
	// 	return nil

}
