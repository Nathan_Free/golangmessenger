package messaging

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

const (
	host   = "localhost"
	port   = 5432
	user   = "postgres"
	dbname = "Messaging"
)

//Message contains a message sent from 1 user to another
type Message struct {
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	TimeSent    string `json:"time_sent"`
	MessageBody string `json:"message_body"`
}

//User contains basic information about a user (generally used to get info about friend)
type User struct {
	Email     string `json:"email"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	UserID    string `json:"user_id"`
}

func main() {
	db := ConnectToDB()
	PingDB(db)
	//createUser(db, "Road", "Runner", "road.runner@aol.com", "beepbeep")
	defer db.Close()

}

//ConnectToDB makes a database connection.
func ConnectToDB() *sql.DB {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"dbname=%s sslmode=disable",
		host, port, user, dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		fmt.Println("error connecting to db: ", err)
		panic(err)
	}
	//defer db.Close()
	return db
}

func PingDB(db *sql.DB) {
	err := db.Ping()
	if err != nil {
		log.Println("could not ping: ", err)
		panic(err)
	}
}

func CreateUser(db *sql.DB, firstName string, lastName string, email string, password string) {
	sqlStatement := `
		INSERT INTO users (first_name, last_name, email, password) VALUES ($1, $2, $3, $4)
		`
	_, err := db.Exec(sqlStatement, firstName, lastName, email, password)
	if err != nil {
		fmt.Println("error creating user: ", err)
		panic(err)
	}

}

func RemoveUser(db *sql.DB, email string) {
	sqlStatement := `
		DELETE FROM users WHERE email = $1
		`
	_, err := db.Exec(sqlStatement, email)
	if err != nil {
		fmt.Println("error removing user: ", err)
		panic(err)
	}

}

//GetIDFromEmail gets the user id in the user table linked to a login email for use in other queries
func GetIDFromEmail(db *sql.DB, email string) (string, error) {
	sqlStatement := `
	 SELECT id FROM users 
	  WHERE email = $1 
		LIMIT(1); 
	 `

	var id string

	userID := db.QueryRow(sqlStatement, email)

	//; err tells program to use value of err in switch
	switch err := userID.Scan(&id); err {
	case sql.ErrNoRows:
		fmt.Println("No rows")
		return "", err
	case nil:
		fmt.Println(id)
		return id, nil
	default:
		fmt.Println(err)
		return "", err
	}

}

//GetFullNameFromEmail combines a user's first and last name into 1 entry given an email address (first acc with that email)
func GetFullNameFromEmail(db *sql.DB, email string) (string, error) {
	sqlStatement := `
	SELECT first_name || ' ' || last_name AS name FROM users WHERE email = $1 LIMIT(1);
	`

	var fullName string

	currentEntryRow := db.QueryRow(sqlStatement, email)

	//; err tells program to use value of err in switch
	switch err := currentEntryRow.Scan(&fullName); err {
	case sql.ErrNoRows:
		fmt.Println("No rows")
		return "", err
	case nil:
		fmt.Println(fullName)
		return fullName, nil
	default:
		//panic(err)
		return "", err
	}
}

//GetFullNameFromID gives a user's first_name last_name from their ID
func GetFullNameFromID(db *sql.DB, id string) (string, error) {
	sqlStatement := `
	SELECT first_name || ' ' || last_name AS name FROM users WHERE id = $1 LIMIT(1);
	`

	var fullName string

	currentEntryRow := db.QueryRow(sqlStatement, id)

	//; err tells program to use value of err in switch
	switch err := currentEntryRow.Scan(&fullName); err {
	case sql.ErrNoRows:
		fmt.Println("No rows")
		return "", err
	case nil:
		fmt.Println(fullName)
		return fullName, nil
	default:
		//panic(err)
		return "", err
	}
}

//GetUserFriendshipsFromEmail allows you to pass in the email address of a user and get their friendships
func GetUserFriendshipsFromEmail(db *sql.DB, userEmail string) (*[]User, error) {
	userID, err := GetIDFromEmail(db, userEmail)

	if err != nil {
		log.Println("Error getting id from email: ", err)
		panic(err)
	} else {
		return GetUserFriendships(db, userID)
	}
}

//GetAllRequestsFrom is meant to get user information about those you sent a friendship request to that hasn't been accepted
func GetAllRequestsFrom(db *sql.DB, userID string) (*[]User, error) {
	var userRequestList []User

	sqlStatement := `
	WITH sentRequests AS (SELECT * FROM friendship_request WHERE sender_id = $1) 
		SELECT email, first_name, last_name,  id FROM users 
			JOIN sentRequests ON users.id = sentRequests.recipient_id;
	`

	rowsRequests, err := db.Query(sqlStatement, userID)
	if err != nil {
		log.Println("error getting users that you sent requests to: ", err)
		return nil, err
	}
	defer rowsRequests.Close()

	for rowsRequests.Next() {
		var user User
		err := rowsRequests.Scan(&user.Email, &user.FirstName, &user.LastName, &user.UserID)
		if err != nil {
			fmt.Println("error getting next user: ", err)
			return &userRequestList, err
		}
		userRequestList = append(userRequestList, user)
	}

	err = rowsRequests.Err()
	if err != nil {
		fmt.Println("error getting rows of requests you sent: ", err)
	}
	return &userRequestList, nil
}

//GetAllRequestsTo is meant to get user information about those that sent you a friendship request that you haven't accepted
func GetAllRequestsTo(db *sql.DB, userID string) (*[]User, error) {
	var userRequestList []User

	sqlStatement := `
	WITH receivedRequests AS (SELECT * FROM friendship_request WHERE recipient_id = $1)
		SELECT email, first_name, last_name,  id FROM users 
			JOIN receivedRequests ON users.id = receivedRequests.sender_id;
	`

	rowsRequests, err := db.Query(sqlStatement, userID)
	if err != nil {
		log.Println("error getting users that sent you requests: ", err)
		return nil, err
	}
	defer rowsRequests.Close()

	for rowsRequests.Next() {
		var user User
		err := rowsRequests.Scan(&user.Email, &user.FirstName, &user.LastName, &user.UserID)
		if err != nil {
			fmt.Println("error getting next user: ", err)
			return &userRequestList, err
		}
		userRequestList = append(userRequestList, user)
	}

	err = rowsRequests.Err()
	if err != nil {
		fmt.Println("error getting rows of requests sent to you: ", err)
	}
	return &userRequestList, nil
}

//TODO: don't show users with pending requests

//GetAllExceptFriends NOT DONE AT ALL: supposed to be for adding new friends
func GetAllExceptFriends(db *sql.DB, userID string) (*[]User, error) {
	var notFriendsList []User

	// sqlStatement := `
	// WITH
	// 		allFriendships AS
	// 		(SELECT sender_id, recipient_id FROM friendship WHERE recipient_id = $1 OR sender_id = $1),
	// 		uniqueFriendships AS
	// 			(SELECT sender_id FROM allFriendships WHERE sender_id != $1
	// 			 UNION SELECT recipient_id FROM allFriendships WHERE recipient_id != $1)
	// 	 SELECT users.email, users.first_name, users.last_name, users.id
	// 		FROM users
	// 		LEFT JOIN uniqueFriendships ON
	// 			users.id = uniqueFriendships.sender_id
	// 				WHERE uniqueFriendships.sender_id IS NULL
	// 				AND users.id != $1;
	// `

	sqlStatement := `
	WITH 
		allFriendshipInvolvement AS
		(SELECT sender_id, recipient_id FROM friendship WHERE recipient_id = $1 OR sender_id = $1
			 UNION SELECT sender_id, recipient_id FROM friendship_request WHERE recipient_id = $1 OR sender_id = $1),
			uniqueFriendships AS 
				(SELECT sender_id FROM allFriendshipInvolvement WHERE sender_id != $1 
				 UNION SELECT recipient_id FROM allFriendshipInvolvement WHERE recipient_id != $1)
			SELECT users.email, users.first_name, users.last_name, users.id 
				FROM users 
					LEFT JOIN uniqueFriendships ON 
						users.id = uniqueFriendships.sender_id 
							WHERE uniqueFriendships.sender_id IS NULL 
							AND users.id != $1;
	
	`

	rowsNotFriendships, err := db.Query(sqlStatement, userID)
	if err != nil {
		fmt.Println("error getting users not in a friendship with user: ", err)
		return nil, err
	}
	defer rowsNotFriendships.Close()

	for rowsNotFriendships.Next() {
		var user User
		err := rowsNotFriendships.Scan(&user.Email, &user.FirstName, &user.LastName, &user.UserID)
		if err != nil {
			fmt.Println("error getting next friendship: ", err)
			return &notFriendsList, err
		}
		notFriendsList = append(notFriendsList, user)
	}

	err = rowsNotFriendships.Err()
	if err != nil {
		fmt.Println("error getting friendship rows", err)
	}
	return &notFriendsList, nil
}

//GetUserFriendships gets all friendships for a given user ID
func GetUserFriendships(db *sql.DB, userID string) (*[]User, error) {
	var friendList []User

	sqlStatement := `
	 WITH allFriendships AS 
	  (SELECT sender_id, recipient_id FROM friendship WHERE recipient_id = $1 OR sender_id = $1)
	  ,uniqueFriendships AS (SELECT sender_id FROM allFriendships WHERE sender_id != $1 UNION 
		SELECT recipient_id FROM allFriendships WHERE recipient_id != $1) 
		SELECT users.email, users.first_name, users.last_name, users.id FROM users 
		INNER JOIN uniqueFriendships ON users.id = uniqueFriendships.sender_id;
	`
	rowsFriendships, err := db.Query(sqlStatement, userID)
	if err != nil {
		fmt.Println("error getting friendships: ", err)
		return nil, err
	}
	defer rowsFriendships.Close()

	//friendnames := make(map[string]string)

	for rowsFriendships.Next() {
		// var firstName string
		// var lastName string
		var user User
		err := rowsFriendships.Scan(&user.Email, &user.FirstName, &user.LastName, &user.UserID)
		if err != nil {
			fmt.Println("error getting next friendship: ", err)
			//panic(err)
			return &friendList, err
		}
		//friendnames[firstName] = lastName
		friendList = append(friendList, user)
	}

	// for firstname, lastname := range friendnames {
	// 		fmt.Println("First Name:", firstname, "Last Name:", lastname)
	// }

	err = rowsFriendships.Err()
	if err != nil {
		fmt.Println("error getting friendship rows", err)
		//panic(err)
	}

	return &friendList, nil
}

//SendMessage sends a message from a sender to a recipient
func SendMessage(db *sql.DB, senderID string, recipientID string, messageBody string) error {
	sqlStatementMessage := `
	INSERT INTO message (sender_id, message_body)
	VALUES ($1, $2)
	RETURNING id
	`
	sqlStatementMessageRecipient := `
	INSERT INTO message_recipient (recipient_id, message_id) VALUES ($1, $2)
	`

	id := 0
	err := db.QueryRow(sqlStatementMessage, senderID, messageBody).Scan(&id)
	if err != nil {
		fmt.Println("error storing message: ", err)
		//panic(err)
		return err
	}

	_, err = db.Exec(sqlStatementMessageRecipient, recipientID, id)
	if err != nil {
		fmt.Println("error sending message: ", err)
		return err
	}

	return nil
}

//ReadLastMessage is to be called when a user reads a message sent to them
func ReadLastMessage(db *sql.DB, recipientID string, messageID string) {
	sqlStatementGetMessage := `
	SELECT id FROM message_recipient
	WHERE recipient_id = $1
	AND message_id = $2
	`

	sqlStatementUpdate := `
	UPDATE message_recipient
	SET has_read = true
	WHERE id = $1
	`

	var id string

	currentEntryRow := db.QueryRow(sqlStatementGetMessage, recipientID, messageID)

	//; err tells program to use value of err in switch
	switch err := currentEntryRow.Scan(&id); err {
	case sql.ErrNoRows:
		fmt.Println("No rows")
	case nil:
		fmt.Println(id)
	default:
		panic(err)
	}

	_, err := db.Exec(sqlStatementUpdate, id)
	if err != nil {
		fmt.Println("couldn't verify message read: ", err)
		panic(err)
	}
}

//SendFriendshipRequest is to be called when a user wants to add another as a friend
func SendFriendshipRequest(db *sql.DB, senderID string, recipientID string) error {
	sqlStatement := `
	INSERT INTO friendship_request (sender_id, recipient_id)
	VALUES ($1, $2)
	`

	_, err := db.Exec(sqlStatement, senderID, recipientID)
	if err != nil {
		fmt.Println("error sending friend request: ", err)
		return err
	}
	return nil
}

//RemoveFriend is called when a user wishes to remove another user from their contact list
func RemoveFriend(db *sql.DB, currentUserID string, friendID string) error {
	sqlStatementRemoveFriend := `
	DELETE FROM friendship 
	WHERE sender_id = $1 AND recipient_id = $2 
	OR sender_id = $2 AND recipient_id = $1;
	`

	_, err := db.Exec(sqlStatementRemoveFriend, currentUserID, friendID)
	if err != nil {
		fmt.Println("error removing friend: ", err)
		return err
	}
	return nil
}

//AcceptFriendshipRequest is to be called when a recipient of a friendship request wants to accept that request
func AcceptFriendshipRequest(db *sql.DB, senderID string, recipientID string) error {
	sqlStatementAcceptFriendship := `
	INSERT INTO friendship (sender_id, recipient_id)
	VALUES ($1, $2)
	`
	_, err := db.Exec(sqlStatementAcceptFriendship, senderID, recipientID)
	if err != nil {
		fmt.Println("error accepting friend request: ", err)
		return err
	}

	RemoveFriendshipRequest(db, senderID, recipientID)
	return nil
}

//RemoveFriendshipRequest is to be called when a recipient of a friendship request wants to deny that request
func RemoveFriendshipRequest(db *sql.DB, senderID string, recipientID string) error {
	sqlStatementRemoveFriendshipRequest := `
	DELETE FROM friendship_request WHERE sender_id = $1 AND recipient_id = $2
	`

	_, err := db.Exec(sqlStatementRemoveFriendshipRequest, senderID, recipientID)
	if err != nil {
		fmt.Println("error denying friend request: ", err)
		return err
	}
	return nil
}

//GetAllMessagesBetweenTwoUsers gets all the messages between 2 users given their ids
func GetAllMessagesBetweenTwoUsers(db *sql.DB, senderID string, recipientID string) (*[]Message, error) {
	var messages []Message
	sqlStatement := `
	 SELECT messages_between_users.time_sent, messages_between_users.message_body, users.first_name, users.last_name 
	  FROM messages_between_users 
		INNER JOIN users ON users.id = messages_between_users.sender_id 
		WHERE messages_between_users.sender_id = $1 
		AND messages_between_users.recipient_id = $2
		OR messages_between_users.sender_id = $2 AND messages_between_users.recipient_id = $1;
	`

	log.Println("senderID and recipientID: ", senderID, recipientID)
	rowsMessages, err := db.Query(sqlStatement, senderID, recipientID)
	defer rowsMessages.Close()
	if err != nil {
		output := fmt.Sprint("error getting messages between two users: ", err.Error())
		//panic(err)
		return &messages, fmt.Errorf(output)
	}

	for rowsMessages.Next() {
		var currentMessage Message
		rowsMessages.Scan(&currentMessage.TimeSent, &currentMessage.MessageBody, &currentMessage.FirstName, &currentMessage.LastName)
		//fmt.Println(timeSent, messageBody)
		messages = append(messages, currentMessage)
		log.Println(currentMessage.FirstName, currentMessage.LastName, currentMessage.MessageBody, currentMessage.TimeSent)
	}

	err = rowsMessages.Err()
	if err != nil {
		//panic(err)
		log.Println("error with message rows: ", err)
		return &messages, err
	}
	log.Println("success in getting messages: ")
	return &messages, nil
}

//GetAllMessagesForUser gets all messages sent by or received from a given user
func GetAllMessagesForUser(db *sql.DB, userID string) {
	//message_transcation view was created
	sqlStatementGetMessages := `
	SELECT time_sent, message_body FROM message_transaction
	WHERE sender_id = $1 OR recipient_id = $1`

	// sqlStatementGetMessagesIDs := `
	// SELECT message_id FROM message_recipient
	// WHERE recipient_id = $1
	// `

	// sqlStatementGetMessagesTo := `
	// SELECT time_sent, message_body FROM message
	// WHERE id = $1
	// `

	rowsMessages, err := db.Query(sqlStatementGetMessages, userID)
	if err != nil {
		fmt.Println("error getting messages involving user {0}", err)
		panic(err)
	}
	defer rowsMessages.Close()

	for rowsMessages.Next() {
		var timeSent string
		var messageBody string
		err := rowsMessages.Scan(&timeSent, &messageBody)
		if err != nil {
			panic(err)
		}
		fmt.Println(timeSent, messageBody)
	}

	err = rowsMessages.Err()
	if err != nil {
		panic(err)
	}

}

//DeleteAllMessagesInvolvingUser doesn't work yet but would help purge that user
func DeleteAllMessagesInvolvingUser(db *sql.DB, userID string) {
	//this won't work unless/until there is a trigger to update 2 tables when updating view
	sqlStatement := `
	DELETE FROM friendship_request WHERE sender_id = $1 OR recipient_id = $1
	`

	_, err := db.Exec(sqlStatement, userID)
	if err != nil {
		fmt.Println("error deleting messages: ", err)
		panic(err)
	}
}

//RegisterUser allows a user to register a new account
func RegisterUser(db *sql.DB, firstName string, lastName string, email string, password string) error {
	sqlStatement := `
	INSERT INTO users (first_name, last_name, email, password) VALUES ($1, $2, $3, $4)
	`

	_, err := db.Exec(sqlStatement, firstName, lastName, email, password)
	if err != nil {
		fmt.Println("error registering user: ", err)
		return err
	}
	return nil
}
